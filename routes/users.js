var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('Rafa\'s Manicure Clients');
});

module.exports = router;
